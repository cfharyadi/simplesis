<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\SchoolController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function () {
    return view("pages.dashboard");
});

Route::resource("schools", SchoolController::class)->only(["index"]);
Route::resource("schools.activities", ActivityController::class);
