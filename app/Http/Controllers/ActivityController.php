<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Lookup;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  School $school
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        return view("pages.school_activities_index", [
            "school" => $school,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  School $school
     * @return \Illuminate\Http\Response
     */
    public function create(School $school)
    {
        return view("pages.school_activities_create_edit", [
            "mode" => "create",
            "formAction" => route("schools.activities.store", [
                "school" => $school,
            ]),
            "jsOptions" => $this->_getJsOptions($school),
            "school" => $school,
            "categories" => Lookup::activityCategory()->get(),
            "organiserCandidates" => User::staff()->get(),
            "organiserIDs" => [],
            "participantCandidates" => User::all(),
            "participantIDs" => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  School $school
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, School $school)
    {
        $validated = $request->validate($this->_getValidationRules());

        DB::transaction(function () use ($school, $validated) {
            $activity = $school->activities()->save(new Activity($validated));

            $activity
                ->organisers()
                ->sync(Arr::get($validated, "organisers", []));

            $activity->attendees()->sync(Arr::get($validated, "attendees", []));
        });

        return redirect(
            route("schools.activities.index", ["school" => $school])
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  School $school
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school, Activity $activity)
    {
        return view("pages.school_activities_create_edit", [
            "mode" => "edit",
            "formAction" => route("schools.activities.update", [
                "school" => $school,
                "activity" => $activity,
            ]),
            "jsOptions" => $this->_getJsOptions($school, $activity),
            "school" => $school,
            "activity" => $activity,
            "categories" => Lookup::activityCategory()->get(),
            "organiserCandidates" => User::staff()->get(),
            "organiserIDs" => $activity
                ->organisers()
                ->pluck("id")
                ->toArray(),
            "participantCandidates" => User::all(),
            "participantIDs" => $activity
                ->attendees()
                ->pluck("id")
                ->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  School $school
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school, Activity $activity)
    {
        $validated = $request->validate($this->_getValidationRules());

        DB::transaction(function () use ($activity, $validated) {
            $activity->update($validated);

            $activity
                ->organisers()
                ->sync(Arr::get($validated, "organisers", []));

            $activity->attendees()->sync(Arr::get($validated, "attendees", []));
        });

        return redirect(
            route("schools.activities.index", [
                "school" => $school,
            ])
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  School $school
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school, Activity $activity)
    {
        $activity->delete();

        return redirect(
            route("schools.activities.index", [
                "school" => $school,
            ])
        );
    }

    /**
     * Helper function to return options for JS
     *
     * @param School $school
     * @param Activity $activity
     * @return array
     */
    private function _getJsOptions(School $school, Activity $activity = null)
    {
        return [
            "mapboxAccessToken" => config("services.mapbox.access_token"),
            "schoolAddress" => $school->address,
            "schoolCoordinates" => [$school->lat, $school->long],
            "activityVenue" => $activity ? $activity->venue : null,
        ];
    }

    /**
     * Helper function to get request validation rules
     *
     * @return array
     */
    private function _getValidationRules()
    {
        return [
            "name" => "required",
            "description" => "required",
            "category_lookup_id" => [
                "required",
                Rule::exists("lookups", "id")->where(function ($query) {
                    return $query->where(
                        "type",
                        Lookup::TYPE_ACTIVITY_CATEGORY
                    );
                }),
            ],
            "organisers" => [
                Rule::exists("users", "id")->where(function ($query) {
                    return $query->where("type", User::TYPE_STAFF);
                }),
            ],
            "attendees" => "sometimes|exists:users,id",
            "date_time" => "date_format:Y-m-d\TH:i|nullable",
            "venue" => "string|nullable",
            "venue_lat" => "string|nullable",
            "venue_long" => "string|nullable",
            "travel_duration" => "integer|nullable",
            "travel_distance" => "integer|nullable",
        ];
    }
}
