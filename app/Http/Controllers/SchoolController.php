<?php

namespace App\Http\Controllers;

use App\Models\School;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("pages.school_index", [
            "schools" => School::with("activities")->get(),
        ]);
    }
}
