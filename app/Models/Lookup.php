<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lookup extends Model
{
    public const TYPE_ACTIVITY_CATEGORY = "ACTIVITY_CATEGORY";

    protected $fillable = ["type", "value", "index"];

    //
    // Query Scope
    //
    public function scopeActivityCategory()
    {
        return $this->where("type", self::TYPE_ACTIVITY_CATEGORY)->orderBy(
            "index"
        );
    }
    //
    // END: Query Scope
    //
}
