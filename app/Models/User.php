<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;

    public const TYPE_STAFF = "STAFF";
    public const TYPE_STUDENT = "STUDENT";
    public const TYPE_PARENT = "PARENT";
    public const TYPE_VOLUNTEER = "VOLUNTEER";
    public const TYPE_OTHER = "OTHER";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ["password", "remember_token"];

    //
    // Query Scope
    //
    public function scopeStaff()
    {
        return $this->where("type", self::TYPE_STAFF);
    }
    //
    // END: Query Scope
    //
}
