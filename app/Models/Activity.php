<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TRAVEL_DISTANCE_KM = "KM";

    protected $fillable = [
        "name",
        "description",
        "category_lookup_id",
        "date_time",
        "venue",
        "venue_lat",
        "venue_long",
        "travel_distance",
        "travel_duration",
    ];

    //
    // Relationship
    //
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function category()
    {
        return $this->belongsTo(Lookup::class, "category_lookup_id");
    }

    public function attendees()
    {
        return $this->belongsToMany(
            User::class,
            "activity_attendee",
            "activity_id",
            "user_id"
        );
    }

    public function organisers()
    {
        return $this->belongsToMany(
            User::class,
            "activity_organiser",
            "activity_id",
            "user_id"
        );
    }
    //
    // END: Relationship
    //

    /**
     * Helper function to return distance in required unit of measurement
     *
     * @param string $unit
     * @return float|integer
     */
    public function getTravelDistance($unit = self::TRAVEL_DISTANCE_KM)
    {
        if ($this->travel_distance === null) {
            return null;
        }

        $value = 0;
        switch ($unit) {
            case self::TRAVEL_DISTANCE_KM:
                $value = round($this->travel_distance / 1000, 2);
                break;
            default:
                $value = $this->travel_distance;
        }

        return $value;
    }
}
