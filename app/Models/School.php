<?php

namespace App\Models;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $fillable = ["name", "address", "lat", "long"];

    //
    // Relationship
    //
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }
    //
    // END: Relationship
    //
}
