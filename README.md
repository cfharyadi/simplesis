## ASSUMPTIONS
1. 'School' entity is required, to store the school location in order to measure distance/duration to venue.
2. Further workflow to manage organisers / attendees (paid/turns up/etc) is out of scope from this exercise.
3. For this exercise purposes, the activity data seed will give Venue address. Venue lat, long, duration and distance can be obtained when we edit the activity.
4. Activity's date time, when available, will be used when querying Map API to get more accurate duration.
5. For this exercise purposes, MAP API key is being saved in .env.example, it shouldn't be in there in real situation


---


## APPLICATION STRUCTURE
1. Model, located in app/Models
2. Controller, located in app/Http/Controllers
3. View, located in resources/views
4. Migration files, located in database/migrations
5. Data seeder files, located in database/seeders
6. Model factory files, located in database/factories; mainly for seeding purpose


---


## SETUP

1. Install docker; https://docs.docker.com/get-docker/
2. Execute below commands in terminal from project folder
```
docker run --rm -it --volume $PWD:/app composer install
```
```
cp .env.example .env
```
```
./vendor/bin/sail up -d
```
Wait ~2 minutes before continue to below command to make sure mysql container is ready, repeat if error occur, most likely the container is not ready
```
./vendor/bin/sail artisan migrate
```
```
./vendor/bin/sail artisan db:seed
```

3. open [localhost](http://localhost/schools) from browser


---


## TEST
1. Execute below commands in terminal from project folder
```
./vendor/bin/sail npm install
```
```
$(npm bin)/cypress run --spec="cypress/integration/activities.spec.js"
```
