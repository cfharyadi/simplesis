<?php

use App\Models\School;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("schools", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("address");
            $table->string("lat")->nullable();
            $table->string("long")->nullable();
            $table->timestamps();
        });

        $schools = [
            [
                "name" => "Chatswood High School",
                "address" => "24 Centennial Ave, Chatswood NSW 2067",
                "lat" => "151.175244",
                "long" => "-33.799954",
            ],
            [
                "name" => "Baulkham Hills High School",
                "address" => "419A Windsor Rd, Baulkham Hills NSW 2153",
                "lat" => "150.990832",
                "long" => "-33.751148",
            ],
        ];

        foreach ($schools as $category) {
            School::create($category);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("schools");
    }
}
