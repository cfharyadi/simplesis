<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("activities", function (Blueprint $table) {
            $table->id();
            $table->foreignId("school_id")->constrained("schools", "id");

            $table->string("name");
            $table->text("description");
            $table
                ->foreignId("category_lookup_id")
                ->constrained("lookups", "id");
            $table->dateTime("date_time")->nullable();
            $table->string("venue")->nullable();

            $table->string("venue_lat")->nullable();
            $table->string("venue_long")->nullable();
            $table
                ->bigInteger("travel_distance")
                ->unsigned()
                ->nullable();
            $table
                ->bigInteger("travel_duration")
                ->unsigned()
                ->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("activities");
    }
}
