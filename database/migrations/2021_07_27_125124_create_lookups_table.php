<?php

use App\Models\Lookup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("lookups", function (Blueprint $table) {
            $table->id();
            $table->string("type");
            $table->string("value");
            $table->smallInteger("index")->unsigned();
            $table->timestamps();
        });

        $activityCategoryLookups = [
            [
                "type" => Lookup::TYPE_ACTIVITY_CATEGORY,
                "value" => "Camp",
                "index" => 1,
            ],
            [
                "type" => Lookup::TYPE_ACTIVITY_CATEGORY,
                "value" => "Co-curricular",
                "index" => 2,
            ],
            [
                "type" => Lookup::TYPE_ACTIVITY_CATEGORY,
                "value" => "Excursion",
                "index" => 3,
            ],
            [
                "type" => Lookup::TYPE_ACTIVITY_CATEGORY,
                "value" => "Sport Event",
                "index" => 4,
            ],
        ];

        foreach ($activityCategoryLookups as $category) {
            Lookup::create($category);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("lookups");
    }
}
