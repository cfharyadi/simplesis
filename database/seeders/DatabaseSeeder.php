<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Lookup;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(100)
            ->create();

        Activity::factory()
            ->count(5)
            ->create();
    }
}
