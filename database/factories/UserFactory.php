<?php

namespace Database\Factories;

use App\Models\User as UserModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "firstname" => $this->faker->name(),
            "lastname" => $this->faker->name(),
            "type" => $this->faker->randomElement([
                UserModel::TYPE_STAFF,
                UserModel::TYPE_STUDENT,
                UserModel::TYPE_PARENT,
                UserModel::TYPE_VOLUNTEER,
                UserModel::TYPE_OTHER,
            ]),
            "email" => $this->faker->unique()->safeEmail(),
            "password" =>
                '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ];
    }
}
