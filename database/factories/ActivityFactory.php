<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\Lookup;
use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "school_id" => $this->faker->randomElement(
                School::pluck("id")->toArray()
            ),
            "name" => $this->faker->sentence(),
            "description" => $this->faker->paragraph(),
            "category_lookup_id" => $this->faker->randomElement(
                Lookup::activityCategory()
                    ->pluck("id")
                    ->toArray()
            ),
            "date_time" => $this->faker->dateTimeBetween(
                "+1 month",
                "+6 month"
            ),
            "venue" => $this->_getVenue(),
        ];
    }

    private function _getVenue()
    {
        $venues = [
            "Sydney Opera House, 2 Macquarie St, Sydney, New South Wales 2000, Australia",
            "Bondi Beach, New South Wales, Australia",
            "Bankstown Aerodrome, New South Wales, Australia",
            "Parliament House, Parliament Dr, Canberra, Australian Capital Territory 2600, Australia",
        ];

        $key = array_rand($venues);

        return $venues[$key];
    }
}
