@extends('layouts.main')

@section('pagehead')

<link href='https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css' rel='stylesheet' />

@endsection

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">{{ $school->name }} - Activities</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group me-2">
            <a href="{{ route('schools.activities.create', ['school' => $school]) }}" 
                class="btn btn-sm btn-primary"
                data-test="create-activity">
                New Activity
            </a>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-striped table-sm" id="activitytable">
        <thead>
            <tr>
                <th scope="col">name</th>
                <th scope="col">Category</th>
                <th scope="col">DateTime</th>
                <th scope="col">Venue</th>
                <th scope="col">Distance (km)</th>
                <th scope="col">Driving ETA (minute)</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($school->activities as $activity)
            <tr>
                <td>{{ Str::of($activity->name)->limit(50) }}</td>
                <td>{{ $activity->category->value }}</td>
                <td>{{ $activity->date_time }}</td>
                <td>{{ $activity->venue }}</td>
                <td>{{ $activity->getTravelDistance() }}</td>
                <td>{{ $activity->travel_duration ?? '' }}</td>
                <td>
                    <a href="{{ route('schools.activities.edit', ['school'=>$school, 'activity'=>$activity]) }}"
                        class="btn btn-sm btn-outline-primary" data-test="button-edit">
                        <i class="bi bi-pencil"></i>
                    </a>

                    <form method="POST"
                        action="{{ route('schools.activities.destroy', ['school' => $school, 'activity' => $activity]) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button class="btn btn-sm btn-outline-danger" data-test="button-delete">
                            <i class="bi bi-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection


@section('pagescript')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#activitytable').DataTable();
    });
</script>

@endsection