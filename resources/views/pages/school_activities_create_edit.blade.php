@extends('layouts.main')

@section('pagehead')

<link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
<link rel="stylesheet"
    href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css"
    type="text/css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<style>
    .mapboxgl-ctrl-geocoder .geocoder-pin-right button.geocoder-icon-close {
        display: none !important;
    }
</style>

@endsection

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">{{ $school->name }} - {{ $mode == 'edit' ? 'Edit':'New' }} Activity</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group me-2">
            <a href="{{ route('schools.activities.index', ['school'=>$school]) }}"
                class="btn btn-sm btn-outline-secondary">Back</a>
        </div>
    </div>
</div>

<form method="POST" action="{{ $formAction }}">
    <div class="row g-3">

        <div class="col-sm-6">

            <div class="mb-3">
                <label class="form-label" for="name">Name</label>
                <input name="name" type='text' class="form-control" id="name"
                    value="{{isset($activity) ? $activity->name : ''}}" data-test="input-name" required/>
            </div>

            <div class="mb-3">
                <label class="form-label" for="description">Description</label>
                <textarea name="description" class="form-control" id="description" rows="2"
                    data-test="input-description" required>{{isset($activity) ? $activity->description : ''}}</textarea>
            </div>

            <div class="mb-3">
                <label class="form-label" for="category">Category</label>
                <select name="category_lookup_id" class="form-control" id="category" data-test="input-category">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" {{ isset($activity) && $activity->category_lookup_id ==
                        $category->id ?
                        'selected="selected"' : ''
                        }}
                        >
                        {{$category->value}}
                    </option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="datetime">Date Time</label>
                <input name="date_time" type='text' class="form-control" id="datetimepicker"
                    value="{{isset($activity) ? $activity->date_time : ''}}" data-test="input-datetime" />
            </div>

            <div class="mb-3">
                <label class="form-label" for="organisers">Organisers</label>
                <select name="organisers[]" multiple class="form-select" aria-label="select organisers" id="organisers"
                    data-test="input-organisers">
                    @foreach($organiserCandidates as $candidate)
                    <option value="{{$candidate->id}}" {{ in_array($candidate->id, $organiserIDs) ?
                        'selected="selected"' : '' }}
                        >
                        {{$candidate->firstname . ' ' . $candidate->lastname . ' (' . strtolower($candidate->type) .
                        ')'}}
                    </option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label" for="attendees">Attendees</label>
                <select name="attendees[]" multiple class="form-select" aria-label="select attendees" id="attendees"
                    data-test="input-attendees">
                    @foreach($participantCandidates as $candidate)
                    <option value="{{$candidate->id}}" {{ in_array($candidate->id, $participantIDs) ?
                        'selected="selected"' : '' }}
                        >
                        {{ $candidate->firstname . ' ' . $candidate->lastname . ' (' . strtolower($candidate->type) .
                        ')' }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-sm-6">

            <div id='map' style='width: 100%; height: 300px;'></div>

            <div class="bg-secondary p-3">
                <p>*input venue address using above map tool (B) to fill these details</p>
                <div class="mb-3">
                    <label class="form-label" for="datetime">Venue</label>
                    <input name="venue" type="text" class="form-control" id="venue" placeholder=""
                        value="{{ isset($activity) ? $activity->venue : '' }}" data-test="input-venue" readonly/>
                </div>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="form-label" for="datetime">Lat</label>
                            <input name="venue_lat" type="text" class="form-control" id="venue_lat" placeholder=""
                                value="{{ isset($activity) ? $activity->venue_lat : '' }}"
                                data-test="input-venuelat" readonly/>
                        </div>
                        <div class="col-sm-6">
                            <label class="form-label" for="datetime">Long</label>
                            <input name="venue_long" type="text" class="form-control" id="venue_long" placeholder=""
                                value="{{ isset($activity) ? $activity->venue_long : '' }}"
                                data-test="input-venuelong" readonly/>
                        </div>
                    </div>

                </div>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-sm-6"><label class="form-label" for="datetime">Travel Distance (km)</label>
                            <input name="travel_distance_km" type="text" class="form-control" id="travel_distance_km"
                                value="{{ isset($activity) ? $activity->getTravelDistance() : '' }}"
                                data-test="input-traveldistance" readonly/>
                            <input name="travel_distance" type="hidden" class="form-control" id="travel_distance"
                                value="{{ isset($activity) ? $activity->travel_distance : '' }}">
                        </div>
                        <div class="col-sm-6"> <label class="form-label" for="datetime">Driving ETA (minute)</label>
                            <input name="travel_duration" type="text" class="form-control" id="travel_duration"
                                placeholder="" value="{{ isset($activity) ? $activity->travel_duration : '' }}"
                                data-test="input-travelduration" readonly/>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <hr class="my-4">

    <button class="w-100 btn btn-primary btn-lg" type="submit" data-test="button-submit">Save</button>

    @if($mode == 'edit')
    <input type="hidden" name="_method" value="PUT">
    @endif
    @csrf
</form>

@endsection

@section('pagescript')
<!-- Load datepicker -->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<!-- Load mapbox -->
<script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>


<!-- Promise polyfill script is required -->
<!-- to use Mapbox GL Geocoder in IE 11. -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

<script>
    $(function () {
        const options = @json($jsOptions);

        // setup datepicker
        const fp = flatpickr($("#datetimepicker"), {
            enableTime: true,
            dateFormat: "Y-m-dTH:i",
            altInput: true,
            altFormat: "Y-m-d H:i",
        });

        // setup mapbox
        mapboxgl.accessToken = options.mapboxAccessToken;
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: options.schoolCoordinates,
            zoom: 10
        });

        // setup mapbox direction
        var directions = new MapboxDirections({
            accessToken: mapboxgl.accessToken,
            profile: 'mapbox/driving',
            steps: false,
            placeholderDestination: 'input venue address',
            controls: { instructions: false, profileSwitcher: false }
        });

        // get venue information when destination is set
        directions.on('destination', function (e) {
            $('#venue').val($('#mapbox-directions-destination-input input').val());
            $('#venue_lat').val(e.feature.geometry.coordinates[0]);
            $('#venue_long').val(e.feature.geometry.coordinates[1]);
        });

        // get route information when available
        directions.on('route', function (e) {
            if (e.route.length < 1) {
                return;
            }

            var route = e.route[0];
            $('#travel_distance').val(parseInt(route.distance));
            $('#travel_distance_km').val(Math.round(route.distance / 10) / 100);
            $('#travel_duration').val(parseInt(route.duration / 60));
        });

        map.addControl(
            directions,
            'top-left'
        );

        // Set Origin and Destination if available
        map.on('load', function () {
            directions.setOrigin(options.schoolAddress);

            if (options.activityVenue) {
                directions.setDestination(options.activityVenue);
            }
        })
    });
</script>
@endsection