let activityName =
    "activity_" + (Math.random() * 10000).toString().substr(0, 4);

context("Activities", () => {
    it("Can create new activity for school", () => {
        cy.visit("/schools/1/activities");
        cy.get("[data-test=create-activity]").click();
        cy.url().should("include", "/schools/1/activities/create");

        cy.get("[data-test=input-name]").type(activityName);
        cy.get("[data-test=input-description]").type(
            activityName + " description"
        );

        cy.get("[data-test=button-submit]").click();
        cy.url().should("include", "/schools/1/activities");
    });

    it("Can edit the activity and change venue", () => {

        cy.visit("/schools/1/activities");
        cy.get("input[type=search]").type(activityName);
        cy.get("#activitytable tbody tr:first").within(() => {
            cy.get("td:first").should("have.text", activityName);
            cy.get("[data-test=button-edit]").click();
        });

        cy.url().should("include", "/edit");

        cy.get(
            "#mapbox-directions-destination-input .mapboxgl-ctrl-geocoder"
        ).within(() => {
            cy.get("input").type("Garigal National Park, Sydney{enter}");
        });

        cy.wait(500);
        
        // These numbers have been pre-calculated from Chatswood HS to Garigal 
        cy.get("[data-test=input-venuelat]").should("have.value", 151.21511);
        cy.get("[data-test=input-venuelong]").should("have.value", -33.719901);
        cy.get("[data-test=input-traveldistance]").should("have.value", 12.55);
        cy.get("[data-test=input-travelduration]").should("have.value", 18);

        cy.get("[data-test=button-submit]").click();

        cy.visit("/schools/1/activities");
        cy.get("input[type=search]").type(activityName);
        cy.get("#activitytable tbody tr:first").within(() => {
            cy.get("td:nth-child(4)").should(
                "include.text",
                "Garigal National Park, Sydney"
            );
        });
    });

    it("Can delete the activity", () => {
        cy.visit("/schools/1/activities");
        cy.get("input[type=search]").type(activityName);
        cy.get("#activitytable tbody tr:first").within(() => {
            cy.get("td:first").should("have.text", activityName);
            cy.get("[data-test=button-delete]").click();
        });

        cy.visit("/schools/1/activities");
        cy.get("input[type=search]").type(activityName);
        cy.get("#activitytable tbody tr:first")
            .find("td:first")
            .should("have.class", "dataTables_empty");
    });
});
